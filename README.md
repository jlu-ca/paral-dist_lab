# paral-dist_lab

English | [中文](README_zh.md)

## Introduction

Lab of parallel and distributed computing course for College of Software, Jilin University.

For more detailed documentation, see [中文](README_zh.md) version.

## Content

- lab1: program with CUDA (?)
- lab2: program with OpenMP
- lab3: program with MPI
- lab4: program with MapReduce
- lab5: VM migration
- lab6: cloud computing
  - lab6A: implement web service as microservices
  - lab6B: deploy microservices with Docker and Kubernetes
