# paral-dist_lab

[English](README.md) | 中文

## 介绍

吉林大学软件学院 并行与分布式计算 实验课

## 内容目录

- lab1: CUDA 编程 (not ready)
- [lab2: OpenMP 编程](lab2/task.md)
- [lab3: MPI 编程](lab3/task.md)
- [lab4: MapReduce 编程](lab4/task.md)
- [lab5: VM 迁移](lab5/task.md)
- [lab6: 云计算实践](lab6/task.md)
  - [lab6A: 以微服务方式实现 web service](lab6/lab6A/task.md)
  - [lab6B: 使用 Docker 和 Kubernetes 进行微服务部署](lab6/lab6B/task.md)

## 说明

每次实验分数为100分满分，最终实验课总成绩将按照以下方式加权平均，总分为100分。

| 实验 | 内容 | 权重 |
| --- | --- | --- |
| lab1 | CUDA 编程      | 0% |
| lab2 | OpenMP 编程    | 20% |
| lab3 | MPI 编程   | 15% |
| lab4 | MapReduce 编程 | 20% |
| lab5 | VM 迁移        | 15% |
| lab6 | 云计算实践      | 30% |

实验在虚拟机中进行，基准虚拟机已配置好大部分环境。为保护实验环境和实验结果，建议每次实验首先将基准虚拟机克隆一份，并在克隆的虚拟机中进行实验；建议每节课结束后对课上操作的虚拟机进行一次快照。

### 多人协作任务

在多人协作任务中，若干同学（人数见实验说明）形成一个小组，小组内每人（或若干人，见子任务实验说明）负责一个子任务，并可以在**完成一轮试验后**交换角色，尝试完成不同的子任务。

你可能会在实验步骤中见到形如 ```Barrier x``` 的标记，这是指**第x号同步栅栏**，意味着必须当小组内**所有成员**均进行到 ```Barrier x``` 后才可以各自继续执行接下来的操作。实验中并不是所有的 Barrier 都必须是强约束，但当你不是完全清楚接下来要执行的指令的意图时，强烈建议你与其他组员按照 Barrier 进行同步。

## 基准虚拟机

[点击此处下载（BT Magnet Link）](magnet:?xt=urn:btih:385749d1f1f1e01e3dfc714dc19ee9fb71aed8fe&dn=pdlab&tr=http%3a%2f%2f1337.abcvg.info%3a80%2fannounce&tr=udp%3a%2f%2f207.241.226.111%3a6969%2fannounce&tr=udp%3a%2f%2f207.241.231.226%3a6969%2fannounce&tr=udp%3a%2f%2f49.12.76.8%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2001%3a1b10%3a1000%3a8101%3a0%3a242%3aac11%3a2%5d%3a6969%2fannounce&tr=udp%3a%2f%2f%5b2a00%3ab700%3a1%3a%3a3%3a1dc%5d%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2a01%3a4f8%3ac012%3a8025%3a%3a1%5d%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2a04%3aac00%3a1%3a3dd8%3a%3a1%3a2710%5d%3a2710%2fannounce&tr=http%3a%2f%2fbt.endpot.com%3a80%2fannounce&tr=http%3a%2f%2fbt.okmp3.ru%3a2710%2fannounce&tr=http%3a%2f%2fbuny.uk%3a6969%2fannounce&tr=http%3a%2f%2fincine.ru%3a6969%2fannounce&tr=http%3a%2f%2fmontreal.nyap2p.com%3a8080%2fannounce&tr=udp%3a%2f%2fmovies.zsw.ca%3a6969%2fannounce&tr=http%3a%2f%2fnyaa.tracker.wf%3a7777%2fannounce&tr=http%3a%2f%2fopen.acgnxtracker.com%3a80%2fannounce&tr=udp%3a%2f%2fretracker.hotplug.ru%3a2710%2fannounce&tr=http%3a%2f%2fshare.camoe.cn%3a8080%2fannounce&tr=http%3a%2f%2ft.acg.rip%3a6699%2fannounce&tr=http%3a%2f%2ft.nyaatracker.com%3a80%2fannounce&tr=http%3a%2f%2ftorrentsmd.com%3a8080%2fannounce&tr=udp%3a%2f%2ftracker.birkenwald.de%3a6969%2fannounce&tr=http%3a%2f%2ftracker.bt4g.com%3a2095%2fannounce&tr=udp%3a%2f%2ftracker.dler.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.dler.org%3a6969%2fannounce&tr=http%3a%2f%2ftracker.electro-torrent.pl%3a80%2fannounce&tr=http%3a%2f%2ftracker.files.fm%3a6969%2fannounce&tr=http%3a%2f%2ftracker.gbitt.info%3a80%2fannounce&tr=http%3a%2f%2ftracker.h0me.cc%3a8880%2fannounce&tr=http%3a%2f%2ftracker.ipv6tracker.ru%3a80%2fannounce&tr=http%3a%2f%2ftracker.mywaifu.best%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce&tr=http%3a%2f%2ftracker.renfei.net%3a8080%2fannounce&tr=udp%3a%2f%2ftracker.swateam.org.uk%3a2710%2fannounce&tr=udp%3a%2f%2ftracker1.bt.moack.co.kr%3a80%2fannounce&tr=udp%3a%2f%2ftracker2.dler.org%3a80%2fannounce&tr=udp%3a%2f%2ftracker4.itzmx.com%3a2710%2fannounce&tr=http%3a%2f%2furaniumhexafluori.de%3a1919%2fannounce&tr=udp%3a%2f%2fwepzone.net%3a6969%2fannounce&tr=http%3a%2f%2fwww.all4nothin.net%3a80%2fannounce.php&tr=http%3a%2f%2fwww.peckservers.com%3a9000%2fannounce&tr=http%3a%2f%2fwww.wareztorrent.com%3a80%2fannounce&tr=https%3a%2f%2f1337.abcvg.info%3a443%2fannounce&tr=https%3a%2f%2ft1.hloli.org%3a443%2fannounce&tr=https%3a%2f%2ftr.burnabyhighstar.com%3a443%2fannounce&tr=https%3a%2f%2ftr.ready4.icu%3a443%2fannounce&tr=https%3a%2f%2ftracker.cloudit.top%3a443%2fannounce&tr=https%3a%2f%2ftracker.foreverpirates.co%3a443%2fannounce&tr=https%3a%2f%2ftracker.gbitt.info%3a443%2fannounce&tr=https%3a%2f%2ftracker.ipfsscan.io%3a443%2fannounce&tr=https%3a%2f%2ftracker.kuroy.me%3a443%2fannounce&tr=https%3a%2f%2ftracker.lilithraws.cf%3a443%2fannounce&tr=https%3a%2f%2ftracker.lilithraws.org%3a443%2fannounce&tr=https%3a%2f%2ftracker.loligirl.cn%3a443%2fannounce&tr=https%3a%2f%2ftracker.renfei.net%3a443%2fannounce&tr=https%3a%2f%2ftracker.tamersunion.org%3a443%2fannounce&tr=https%3a%2f%2ftracker1.520.jp%3a443%2fannounce&tr=https%3a%2f%2ftrackers.mlsub.net%3a443%2fannounce&tr=https%3a%2f%2fwww.peckservers.com%3a9443%2fannounce&tr=udp%3a%2f%2f107.182.30.76.16clouds.com%3a6969%2fannounce&tr=udp%3a%2f%2f119.28.71.45%3a8080%2fannounce&tr=udp%3a%2f%2f184.105.151.166%3a6969%2fannounce&tr=udp%3a%2f%2f1c.premierzal.ru%3a6969%2fannounce&tr=http%3a%2f%2f207.241.226.111%3a6969%2fannounce&tr=http%3a%2f%2f207.241.231.226%3a6969%2fannounce&tr=udp%3a%2f%2f46.17.46.112%3a8080%2fannounce&tr=http%3a%2f%2f49.12.76.8%3a8080%2fannounce&tr=udp%3a%2f%2f52.58.128.163%3a6969%2fannounce&tr=udp%3a%2f%2f6.pocketnet.app%3a6969%2fannounce&tr=udp%3a%2f%2f6ahddutb1ucc3cp.ru%3a6969%2fannounce&tr=udp%3a%2f%2f91.216.110.52%3a451%2fannounce&tr=http%3a%2f%2f%5b2001%3a1b10%3a1000%3a8101%3a0%3a242%3aac11%3a2%5d%3a6969%2fannounce&tr=udp%3a%2f%2f%5b2001%3a470%3a1%3a189%3a0%3a1%3a2%3a3%5d%3a6969%2fannounce&tr=http%3a%2f%2f%5b2a00%3ab700%3a1%3a%3a3%3a1dc%5d%3a8080%2fannounce&tr=http%3a%2f%2f%5b2a01%3a4f8%3ac012%3a8025%3a%3a1%5d%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2a03%3a7220%3a8083%3acd00%3a%3a1%5d%3a451%2fannounce&tr=http%3a%2f%2f%5b2a04%3aac00%3a1%3a3dd8%3a%3a1%3a2710%5d%3a2710%2fannounce&tr=udp%3a%2f%2f%5b2a0f%3ae586%3af%3af%3a%3a81%5d%3a6969%2fannounce&tr=udp%3a%2f%2faarsen.me%3a6969%2fannounce&tr=udp%3a%2f%2facxx.de%3a6969%2fannounce&tr=udp%3a%2f%2faegir.sexy%3a6969%2fannounce&tr=udp%3a%2f%2fboysbitte.be%3a6969%2fannounce&tr=udp%3a%2f%2fbt.ktrackers.com%3a6666%2fannounce&tr=udp%3a%2f%2fbt1.archive.org%3a6969%2fannounce&tr=udp%3a%2f%2fbt2.archive.org%3a6969%2fannounce&tr=udp%3a%2f%2fcity21.pk%3a6969%2fannounce&tr=udp%3a%2f%2fconcen.org%3a6969%2fannounce&tr=udp%3a%2f%2fd40969.acod.regrucolo.ru%3a6969%2fannounce&tr=udp%3a%2f%2fec2-18-191-163-220.us-east-2.compute.amazonaws.com%3a6969%2fannounce&tr=udp%3a%2f%2fepider.me%3a6969%2fannounce&tr=udp%3a%2f%2fexodus.desync.com%3a6969%2fannounce&tr=udp%3a%2f%2ffe.dealclub.de%3a6969%2fannounce&tr=udp%3a%2f%2ffh2.cmp-gaming.com%3a6969%2fannounce&tr=udp%3a%2f%2ffree.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2ffreedomalternative.com%3a6969%2fannounce&tr=udp%3a%2f%2fhtz3.noho.st%3a6969%2fannounce&tr=udp%3a%2f%2fipv6.fuuuuuck.com%3a6969%2fannounce&tr=udp%3a%2f%2flloria.fr%3a6969%2fannounce&tr=udp%3a%2f%2fmail.artixlinux.org%3a6969%2fannounce&tr=udp%3a%2f%2fmail.segso.net%3a6969%2fannounce&tr=udp%3a%2f%2fmoonburrow.club%3a6969%2fannounce&tr=http%3a%2f%2fmovies.zsw.ca%3a6969%2fannounce&tr=udp%3a%2f%2fnew-line.net%3a6969%2fannounce&tr=udp%3a%2f%2fodd-hd.fr%3a6969%2fannounce&tr=udp%3a%2f%2foh.fuuuuuck.com%3a6969%2fannounce&tr=udp%3a%2f%2fopen.demonii.com%3a1337%2fannounce&tr=udp%3a%2f%2fopen.dstud.io%3a6969%2fannounce&tr=udp%3a%2f%2fopen.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fopen.stealth.si%3a80%2fannounce&tr=udp%3a%2f%2fopen.tracker.ink%3a6969%2fannounce&tr=udp%3a%2f%2fopen.u-p.pw%3a6969%2fannounce&tr=udp%3a%2f%2fopentor.org%3a2710%2fannounce&tr=udp%3a%2f%2fopentracker.io%3a6969%2fannounce&tr=udp%3a%2f%2fp4p.arenabg.com%3a1337%2fannounce&tr=udp%3a%2f%2fprivate.anonseed.com%3a6969%2fannounce&tr=udp%3a%2f%2fpsyco.fr%3a6969%2fannounce&tr=udp%3a%2f%2fpublic-tracker.cf%3a6969%2fannounce&tr=udp%3a%2f%2fpublic.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fpublic.tracker.vraphim.com%3a6969%2fannounce&tr=http%3a%2f%2fretracker.hotplug.ru%3a2710%2fannounce&tr=udp%3a%2f%2fretracker01-msk-virt.corbina.net%3a80%2fannounce&tr=udp%3a%2f%2frun-2.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2frun.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fryjer.com%3a6969%2fannounce&tr=udp%3a%2f%2fsabross.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fsanincode.com%3a6969%2fannounce&tr=udp%3a%2f%2fsoundmovie.ru%3a6969%2fannounce&tr=udp%3a%2f%2fstatic.54.161.216.95.clients.your-server.de%3a6969%2fannounce&tr=udp%3a%2f%2fsu-data.com%3a6969%2fannounce&tr=udp%3a%2f%2ftamas3.ynh.fr%3a6969%2fannounce&tr=udp%3a%2f%2fthinking.duckdns.org%3a6969%2fannounce&tr=udp%3a%2f%2fthouvenin.cloud%3a6969%2fannounce&tr=udp%3a%2f%2ftk1.trackerservers.com%3a8080%2fannounce&tr=udp%3a%2f%2ftk1v6.trackerservers.com%3a8080%2fannounce&tr=udp%3a%2f%2ftorrents.artixlinux.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker-udp.gbitt.info%3a80%2fannounce&tr=udp%3a%2f%2ftracker.0x7c0.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.4.babico.name.tr%3a3131%2fannounce&tr=udp%3a%2f%2ftracker.artixlinux.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.auctor.tv%3a6969%2fannounce&tr=http%3a%2f%2ftracker.birkenwald.de%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.bittor.pw%3a1337%2fannounce&tr=udp%3a%2f%2ftracker.ccp.ovh%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.cubonegro.lol%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.cyberia.is%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.ddunlimited.net%3a6969%2fannounce&tr=http%3a%2f%2ftracker.dler.com%3a6969%2fannounce&tr=http%3a%2f%2ftracker.dler.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.dump.cl%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.filemail.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.fnix.net%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.moeking.me%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a6969%2fannounce&tr=http%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce&tr=udp%3a%2f%2ftracker.qu.ax%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.skyts.net%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.srv00.com%3a6969%2fannounce&tr=http%3a%2f%2ftracker.swateam.org.uk%3a2710%2fannounce&tr=udp%3a%2f%2ftracker.t-rb.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.theoks.net%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.therarbg.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.tiny-vps.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.torrent.eu.org%3a451%2fannounce&tr=udp%3a%2f%2ftracker.tryhackx.org%3a6969%2fannounce&tr=http%3a%2f%2ftracker1.bt.moack.co.kr%3a80%2fannounce&tr=udp%3a%2f%2ftracker1.myporn.club%3a9337%2fannounce&tr=udp%3a%2f%2ftracker2.dler.com%3a80%2fannounce&tr=http%3a%2f%2ftracker2.dler.org%3a80%2fannounce&tr=udp%3a%2f%2ftracker2.itzmx.com%3a6961%2fannounce&tr=udp%3a%2f%2ftracker3.itzmx.com%3a6961%2fannounce&tr=http%3a%2f%2ftracker4.itzmx.com%3a2710%2fannounce&tr=udp%3a%2f%2fttk2.nbaonlineservice.com%3a6969%2fannounce&tr=udp%3a%2f%2fu4.trakx.crim.ist%3a1337%2fannounce&tr=udp%3a%2f%2fu6.trakx.crim.ist%3a1337%2fannounce&tr=udp%3a%2f%2fuploads.gamecoast.net%3a6969%2fannounce&tr=udp%3a%2f%2fv1046920.hosted-by-vdsina.ru%3a6969%2fannounce&tr=udp%3a%2f%2fv2.iperson.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fvm3268418.24ssd.had.wf%3a6969%2fannounce&tr=http%3a%2f%2fwepzone.net%3a6969%2fannounce&tr=udp%3a%2f%2fyahor.of.by%3a6969%2fannounce&tr=ws%3a%2f%2fhub.bugout.link%3a80%2fannounce&tr=wss%3a%2f%2ftracker.openwebtorrent.com%3a443%2fannounce&tr=http%3a%2f%2f59.72.109.34%3a9000%2fannounce) 该链接请使用Bittorrent软件下载，可以选择[点击此处下载qbittorrent](https://www.fosshub.com/qBittorrent.html)，使用qBittorrent Windows x64 (qt6)

[点击此处下载 提取码: wh4w](https://pan.baidu.com/s/1Sg6fszJHGG7z8oC7ZJnBmQ)

[点击此处下载 分流链接 访问码：wnw1](https://cloud.189.cn/web/share?code=fUrQR32A7vAn)

### 基本配置

| 名称 | 信息 |
| --- | --- |
| CPUs | 4 |
| Memory | 4GB |
| Disk | 50GB |
| OS | CentOS 7 |
| hostname | pd001 |

### 账户信息

| username | password |
| --- | --- |
| pd | 123456 |
| root | 123456 |

### 网络配置 enp0s3

- subnet: 192.168.1.0/24
- ipaddr: 192.168.1.111
- gateway: 192.168.1.1

_请根据实际情况对网络配置和MAC地址进行适当的修改。_

注：防火墙和SELinux已经关闭。
