# lab3 - MPI 编程

[实验课总目录](../README_zh.md)

[TOC]

## 实验说明

本次实验主要内容为在多机环境下使用 MPI 编程。

本实验分为多人协作任务部分和单人任务部分。多人协作部分需要**3人一组**，共同完成多节点 MPI 运行环境配置任务。单人部分为实验主要内容，每人均需要独立进行 MPI 编程。单人部分代码编写完成后，使用多人协作部分的环境运行自己编写的代码。

## 评分标准

本次实验满分100分，将根据实验的完成度与实验步骤中各项任务的分值进行打分。

### 采分点

| 任务 | 采分点 | 分数 |
|-|-|-|
|准备工作| 部署 MPI 环境 | 20 |
|编写执行 MPI 程序| hello world | 30 |
|编写执行 MPI 程序| 矩阵加法 | 30 |
|编写执行 MPI 程序| 矩阵乘法 | 20 |
|合计| | 100 |

## 虚拟机环境

虚拟机内环境已配置好，以下不需要执行，仅供参考。

    ```shell
    yum -y install gcc gcc-c++

    wget https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.2.tar.gz
    tar zxvf openmpi-4.1.2.tar.gz

    cd openmpi-4.1.2/

    mkdir -p /usr/local/openmpi-4.1.2
    ./configure --prefix=/usr/local/openmpi-4.1.2
    make all install

    echo "export PATH=/usr/local/openmpi-4.1.2/bin:\$PATH" >> ~/.bashrc
    echo "export LD_LIBRARY_PATH=/usr/local/openmpi-4.1.2/lib:\$LD_LIBRARY_PATH" >> ~/.bashrc
    source ~/.bashrc
    ```

## 实验内容

### 准备工作（多人协作任务，20分）

三人一组，开始前请先确定每位同学的序号（1、2、3）。

在开始各自的单人任务之前，**请先在每台虚拟机上各自进行以下准备工作**：

1. 更改基准虚拟机的MAC地址，将后两位替换为机器号的16进制编号

2. 在虚拟机内，修改hostname和ip等信息

        ```shell
        # 将<hostname>替换为pd+三位十进制机器号（不足三位前边补0），如pd001
        hostnamectl set-hostname <hostname>

        # 修改/etc/sysconfig/network-scripts/ifcfg-enp0s3中的IPADDR项，改为192.168.1.x，其中x是机器号

        # 重启
        reboot
        ```

#### **Barrier 1**

3. 创建 SSH 密钥并拷贝到同组其他机器中

        ```shell
        # 按照提示创建密钥
        ssh-keygen

        # 运行两次以下命令，分别将<ip-peer>更换为同组其他机器的ip
        ssh-copy-id pd@<ip-peer>

        # 运行两次以下命令，ssh到同组其他机器并退回，分别将<ip-peer>更换为同组其他机器的ip
        ssh pd@<ip-peer>
        ```

4. 修改 hosts 文件

        ```shell
        # 编辑/etc/hosts
        # 添加以下内容，分别将<ip01>、<ip02>、<ip03>替换为组内1、2、3号同学机器的ip
        <ip01> node01
        <ip02> node02
        <ip03> node03
        ```

#### **Barrier 2**

### 实验步骤（单人任务，80分）

#### **Step 1**: 编写 MPI 程序

请分别按照以下要求编写 MPI 程序：

1. **(30分)** 使用 MPI 编写多进程 hello world 例程（每个进程均能够输出 hello world 和线程号）
2. **(30分)** 使用 MPI 编写多线程矩阵*加法*例程
3. **(20分)** 使用 MPI 编写多线程矩阵*乘法*例程

注意，由于以上任务分值递减，请按照**从前到后**的顺序完成以上任务。

每一项编程任务均需要能够通过编译并运行得到结果（Step 2、3）才能得到分数。

#### **Step 2**: 编译程序

得分必要项

    ```shell
    # 将test.c替换为你的代码文件名，test.o是输出的可执行文件名
    mpicc -o test.o test.c
    ```

#### **Step 3**: 执行程序并查看结果

    ```shell
    # mpirun的使用方法摘录

    # mpirun [OPTION]...  [PROGRAM]...
    # -c|-np|--np <arg0>       Number of processes to run
    # -H|-host|--host <arg0>   List of hosts to invoke processes on
    #
    # A "slot" is the Open MPI term for an allocatable unit where we can
    # launch a process.  The number of slots available are defined by the
    # environment in which Open MPI processes are run:
    #   1. Hostfile, via "slots=N" clauses (N defaults to number of
    #      processor cores if not provided)
    #   2. The --host command line parameter, via a ":N" suffix on the
    #      hostname (N defaults to 1 if not provided)
    #   3. Resource manager (e.g., SLURM, PBS/Torque, LSF, etc.)
    #   4. If none of a hostfile, the --host command line parameter, or an
    #      RM is present, Open MPI defaults to the number of processor cores
    ```

得分必要项

    ```shell
    # 在单机模式下运行上一步中生成的可执行文件，np值根据虚拟机核数调整
    mpirun -np 4 ./test.o
    ```

得分可选项（关联多人协作任务20分）

    ```shell
    # 将上一步生成的可执行文件拷贝到每台机器的同一位置，分别将<ip-peer>更换为同组其他机器的ip
    scp ./test.o pd@<ip-peer>:~/
    # 另一种方案是将可执行文件放在NFS服务器中，每台机器都将其挂载到相同路径下

    # 在多机模式下运行上一步中生成的可执行文件
    mpirun --mca btl_tcp_if_include 192.168.1.0/24 -np 8 -host node01:4,node02:4,node03:4 ./test.o
    ```
