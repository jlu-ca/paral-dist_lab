# lab5 - VM 迁移

[实验课总目录](../README_zh.md)

## 实验说明

本次实验主要任务为使用 kvm 对正在运行的虚拟机进行跨节点迁移

```text
NFS-server: 用于存储要迁移的VM客户机的磁盘镜像
server2: 迁移的目标宿主机
server3: 迁移的源宿主机
```

```topo
Topology

            -----------------
            |   NFS-server  |   <-- qemu disk img here
            -----------------  
                    |
        -------------------------
        |                       |
    ---------               ---------
    |server2|   <--VM---    |server3|
    ---------               ---------
```

本实验任务为多人协作任务，包括三项子任务。**开始子任务之前请先完成本页面的准备工作**。**3人一组，每人负责一项子任务**，子任务分别为：

- [1号任务（1人）：建立 NFS 服务器](sub_01.md)
- [2号任务（1人）：配置迁移目标主机](sub_02.md)
- [3号任务（1人）：配置迁移源主机并进行迁移](sub_03.md)

具体任务请进入对应文档查看

## 评分标准

本次实验满分100分，将根据完成实验后的成果演示打分。三人分别负责各自任务完成一轮实验即可获得基础分数70分；如果能够进行角色交换，体验不同任务，可根据每位组员对整体流程的把握情况酌情给予更高分数

### 采分点

| 任务 | 采分点 | 分数 |
|-|-|-|
|VM 迁移| 完成一轮 | 70 |
|VM 迁移| 交换角色完成多轮 | 30 |
|合计| | 100 |

## 环境

虚拟机内环境已配置好，以下不需要执行，仅供参考

```shell
# Check

grep -Eoc '(vmx|svm)' /proc/cpuinfo
# output > 0


# Install

yum install qemu-kvm libvirt libvirt-python libguestfs-tools virt-install virt-manager libvirt-client
systemctl enable libvirtd && systemctl start libvirtd

yum install nfs-utils rpcbind
systemctl enable rpcbind
systemctl enable nfs-server
systemctl enable nfs-idmap
systemctl start rpcbind
systemctl start nfs-server
systemctl start nfs-idmap
```

## 准备工作

在开始各自的实验之前，**请先在每台虚拟机上进行以下准备工作**

1. 更改基准虚拟机的MAC地址，将后两位替换为机器号的16进制编号

2. 在虚拟机内，修改hostname和ip等信息

    ```shell
    # 将<hostname>替换为pd+三位十进制机器号（不足三位前边补0），如pd001
    hostnamectl set-hostname <hostname>

    # 修改/etc/sysconfig/network-scripts/ifcfg-enp0s3中的IPADDR项，改为192.168.1.x，其中x是机器号

    # 重启
    reboot
    ```

3. 建议实验全部操作在root账户下执行

    ```shell
    su root
    ```
