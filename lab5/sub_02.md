# lab5 - VM 迁移 - 2号任务

[上一级任务：lab5](task.md)

## 实验说明

3人一组，本文档为2号子任务，需要1名同学：配置迁移目标主机

## 实验步骤

### **Barrier 1**

### **Step 1**: 挂载相应资源

```shell
# 挂载1号任务共享的目录，将<ip01>替换为1号任务所在机器的ip
mount -t nfs -o nolock <ip01>:/var/lib/libvirt-img/images /var/lib/libvirt/images
```

### **Barrier 2**

### **Step 2**: 将3号任务同学密钥加入本地

打开 ```~/.ssh/authorized_keys``` 文件（不存在就创建一个），在文件末尾起一行，将3号同学提供的内容粘贴进来

**或者**在3号机器上使用 ```ssh-copy-id root@<ip02>``` 命令自动完成

### **Barrier 3**

### **Barrier 4**

### **Step 3**: 查看虚拟机迁移是否成功

```shell
virsh list
```
