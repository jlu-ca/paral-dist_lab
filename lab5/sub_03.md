# lab5 - VM 迁移 - 3号任务

[上一级任务：lab5](task.md)

## 实验说明

3人一组，本文档为3号子任务，需要1名同学：配置迁移源主机并进行迁移

## 实验步骤

### **Barrier 1**

### **Step 1**: 获取系统镜像资源

#### 若已提供NFS服务器和镜像文件

```shell
# 挂载公共系统镜像目录，将<osimg-dir>替换为课上老师提供的目录地址
mkdir -p /mnt/imgs
mount -t nfs -o nolock,vers=4 <osimg-dir> /mnt/imgs

# 将公共系统镜像拷贝到本地
cp /mnt/imgs/ubuntu-20.04.3-live-server-amd64.iso ~/
```

#### 否则

1. 进入[吉林大学开源镜像站/Ubuntu-Releases/18.04.6/ubuntu-18.04.6-desktop-amd64.iso](http://mirrors.jlu.edu.cn/ubuntu-releases/18.04.6/ubuntu-18.04.6-desktop-amd64.iso)下载该示例镜像，或者使用[BT Magnet Link下载Ubuntu镜像文件](magnet:?xt=urn:btih:bc26c6bc83d0ca1a7bf9875df1ffc3fed81ff555&dn=ubuntu-18.04.6-desktop-amd64.iso&tr=https%3a%2f%2ftorrent.ubuntu.com%2fannounce&tr=https%3a%2f%2fipv6.torrent.ubuntu.com%2fannounce&tr=http%3a%2f%2f1337.abcvg.info%3a80%2fannounce&tr=udp%3a%2f%2f207.241.226.111%3a6969%2fannounce&tr=udp%3a%2f%2f207.241.231.226%3a6969%2fannounce&tr=udp%3a%2f%2f49.12.76.8%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2001%3a1b10%3a1000%3a8101%3a0%3a242%3aac11%3a2%5d%3a6969%2fannounce&tr=udp%3a%2f%2f%5b2a00%3ab700%3a1%3a%3a3%3a1dc%5d%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2a01%3a4f8%3ac012%3a8025%3a%3a1%5d%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2a04%3aac00%3a1%3a3dd8%3a%3a1%3a2710%5d%3a2710%2fannounce&tr=http%3a%2f%2fbt.endpot.com%3a80%2fannounce&tr=http%3a%2f%2fbt.okmp3.ru%3a2710%2fannounce&tr=http%3a%2f%2fbuny.uk%3a6969%2fannounce&tr=http%3a%2f%2fincine.ru%3a6969%2fannounce&tr=http%3a%2f%2fmontreal.nyap2p.com%3a8080%2fannounce&tr=udp%3a%2f%2fmovies.zsw.ca%3a6969%2fannounce&tr=http%3a%2f%2fnyaa.tracker.wf%3a7777%2fannounce&tr=http%3a%2f%2fopen.acgnxtracker.com%3a80%2fannounce&tr=udp%3a%2f%2fretracker.hotplug.ru%3a2710%2fannounce&tr=http%3a%2f%2fshare.camoe.cn%3a8080%2fannounce&tr=http%3a%2f%2ft.acg.rip%3a6699%2fannounce&tr=http%3a%2f%2ft.nyaatracker.com%3a80%2fannounce&tr=http%3a%2f%2ftorrentsmd.com%3a8080%2fannounce&tr=udp%3a%2f%2ftracker.birkenwald.de%3a6969%2fannounce&tr=http%3a%2f%2ftracker.bt4g.com%3a2095%2fannounce&tr=udp%3a%2f%2ftracker.dler.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.dler.org%3a6969%2fannounce&tr=http%3a%2f%2ftracker.electro-torrent.pl%3a80%2fannounce&tr=http%3a%2f%2ftracker.files.fm%3a6969%2fannounce&tr=http%3a%2f%2ftracker.gbitt.info%3a80%2fannounce&tr=http%3a%2f%2ftracker.h0me.cc%3a8880%2fannounce&tr=http%3a%2f%2ftracker.ipv6tracker.ru%3a80%2fannounce&tr=http%3a%2f%2ftracker.mywaifu.best%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce&tr=http%3a%2f%2ftracker.renfei.net%3a8080%2fannounce&tr=udp%3a%2f%2ftracker.swateam.org.uk%3a2710%2fannounce&tr=udp%3a%2f%2ftracker1.bt.moack.co.kr%3a80%2fannounce&tr=udp%3a%2f%2ftracker2.dler.org%3a80%2fannounce&tr=udp%3a%2f%2ftracker4.itzmx.com%3a2710%2fannounce&tr=http%3a%2f%2furaniumhexafluori.de%3a1919%2fannounce&tr=udp%3a%2f%2fwepzone.net%3a6969%2fannounce&tr=http%3a%2f%2fwww.all4nothin.net%3a80%2fannounce.php&tr=http%3a%2f%2fwww.peckservers.com%3a9000%2fannounce&tr=http%3a%2f%2fwww.wareztorrent.com%3a80%2fannounce&tr=https%3a%2f%2f1337.abcvg.info%3a443%2fannounce&tr=https%3a%2f%2ft1.hloli.org%3a443%2fannounce&tr=https%3a%2f%2ftr.burnabyhighstar.com%3a443%2fannounce&tr=https%3a%2f%2ftr.ready4.icu%3a443%2fannounce&tr=https%3a%2f%2ftracker.cloudit.top%3a443%2fannounce&tr=https%3a%2f%2ftracker.foreverpirates.co%3a443%2fannounce&tr=https%3a%2f%2ftracker.gbitt.info%3a443%2fannounce&tr=https%3a%2f%2ftracker.ipfsscan.io%3a443%2fannounce&tr=https%3a%2f%2ftracker.kuroy.me%3a443%2fannounce&tr=https%3a%2f%2ftracker.lilithraws.cf%3a443%2fannounce&tr=https%3a%2f%2ftracker.lilithraws.org%3a443%2fannounce&tr=https%3a%2f%2ftracker.loligirl.cn%3a443%2fannounce&tr=https%3a%2f%2ftracker.renfei.net%3a443%2fannounce&tr=https%3a%2f%2ftracker.tamersunion.org%3a443%2fannounce&tr=https%3a%2f%2ftracker1.520.jp%3a443%2fannounce&tr=https%3a%2f%2ftrackers.mlsub.net%3a443%2fannounce&tr=https%3a%2f%2fwww.peckservers.com%3a9443%2fannounce&tr=udp%3a%2f%2f107.182.30.76.16clouds.com%3a6969%2fannounce&tr=udp%3a%2f%2f119.28.71.45%3a8080%2fannounce&tr=udp%3a%2f%2f184.105.151.166%3a6969%2fannounce&tr=udp%3a%2f%2f1c.premierzal.ru%3a6969%2fannounce&tr=http%3a%2f%2f207.241.226.111%3a6969%2fannounce&tr=http%3a%2f%2f207.241.231.226%3a6969%2fannounce&tr=udp%3a%2f%2f46.17.46.112%3a8080%2fannounce&tr=http%3a%2f%2f49.12.76.8%3a8080%2fannounce&tr=udp%3a%2f%2f52.58.128.163%3a6969%2fannounce&tr=udp%3a%2f%2f6.pocketnet.app%3a6969%2fannounce&tr=udp%3a%2f%2f6ahddutb1ucc3cp.ru%3a6969%2fannounce&tr=udp%3a%2f%2f91.216.110.52%3a451%2fannounce&tr=http%3a%2f%2f%5b2001%3a1b10%3a1000%3a8101%3a0%3a242%3aac11%3a2%5d%3a6969%2fannounce&tr=udp%3a%2f%2f%5b2001%3a470%3a1%3a189%3a0%3a1%3a2%3a3%5d%3a6969%2fannounce&tr=http%3a%2f%2f%5b2a00%3ab700%3a1%3a%3a3%3a1dc%5d%3a8080%2fannounce&tr=http%3a%2f%2f%5b2a01%3a4f8%3ac012%3a8025%3a%3a1%5d%3a8080%2fannounce&tr=udp%3a%2f%2f%5b2a03%3a7220%3a8083%3acd00%3a%3a1%5d%3a451%2fannounce&tr=http%3a%2f%2f%5b2a04%3aac00%3a1%3a3dd8%3a%3a1%3a2710%5d%3a2710%2fannounce&tr=udp%3a%2f%2f%5b2a0f%3ae586%3af%3af%3a%3a81%5d%3a6969%2fannounce&tr=udp%3a%2f%2faarsen.me%3a6969%2fannounce&tr=udp%3a%2f%2facxx.de%3a6969%2fannounce&tr=udp%3a%2f%2faegir.sexy%3a6969%2fannounce&tr=udp%3a%2f%2fboysbitte.be%3a6969%2fannounce&tr=udp%3a%2f%2fbt.ktrackers.com%3a6666%2fannounce&tr=udp%3a%2f%2fbt1.archive.org%3a6969%2fannounce&tr=udp%3a%2f%2fbt2.archive.org%3a6969%2fannounce&tr=udp%3a%2f%2fcity21.pk%3a6969%2fannounce&tr=udp%3a%2f%2fconcen.org%3a6969%2fannounce&tr=udp%3a%2f%2fd40969.acod.regrucolo.ru%3a6969%2fannounce&tr=udp%3a%2f%2fec2-18-191-163-220.us-east-2.compute.amazonaws.com%3a6969%2fannounce&tr=udp%3a%2f%2fepider.me%3a6969%2fannounce&tr=udp%3a%2f%2fexodus.desync.com%3a6969%2fannounce&tr=udp%3a%2f%2ffe.dealclub.de%3a6969%2fannounce&tr=udp%3a%2f%2ffh2.cmp-gaming.com%3a6969%2fannounce&tr=udp%3a%2f%2ffree.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2ffreedomalternative.com%3a6969%2fannounce&tr=udp%3a%2f%2fhtz3.noho.st%3a6969%2fannounce&tr=udp%3a%2f%2fipv6.fuuuuuck.com%3a6969%2fannounce&tr=udp%3a%2f%2flloria.fr%3a6969%2fannounce&tr=udp%3a%2f%2fmail.artixlinux.org%3a6969%2fannounce&tr=udp%3a%2f%2fmail.segso.net%3a6969%2fannounce&tr=udp%3a%2f%2fmoonburrow.club%3a6969%2fannounce&tr=http%3a%2f%2fmovies.zsw.ca%3a6969%2fannounce&tr=udp%3a%2f%2fnew-line.net%3a6969%2fannounce&tr=udp%3a%2f%2fodd-hd.fr%3a6969%2fannounce&tr=udp%3a%2f%2foh.fuuuuuck.com%3a6969%2fannounce&tr=udp%3a%2f%2fopen.demonii.com%3a1337%2fannounce&tr=udp%3a%2f%2fopen.dstud.io%3a6969%2fannounce&tr=udp%3a%2f%2fopen.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fopen.stealth.si%3a80%2fannounce&tr=udp%3a%2f%2fopen.tracker.ink%3a6969%2fannounce&tr=udp%3a%2f%2fopen.u-p.pw%3a6969%2fannounce&tr=udp%3a%2f%2fopentor.org%3a2710%2fannounce&tr=udp%3a%2f%2fopentracker.io%3a6969%2fannounce&tr=udp%3a%2f%2fp4p.arenabg.com%3a1337%2fannounce&tr=udp%3a%2f%2fprivate.anonseed.com%3a6969%2fannounce&tr=udp%3a%2f%2fpsyco.fr%3a6969%2fannounce&tr=udp%3a%2f%2fpublic-tracker.cf%3a6969%2fannounce&tr=udp%3a%2f%2fpublic.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fpublic.tracker.vraphim.com%3a6969%2fannounce&tr=http%3a%2f%2fretracker.hotplug.ru%3a2710%2fannounce&tr=udp%3a%2f%2fretracker01-msk-virt.corbina.net%3a80%2fannounce&tr=udp%3a%2f%2frun-2.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2frun.publictracker.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fryjer.com%3a6969%2fannounce&tr=udp%3a%2f%2fsabross.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fsanincode.com%3a6969%2fannounce&tr=udp%3a%2f%2fsoundmovie.ru%3a6969%2fannounce&tr=udp%3a%2f%2fstatic.54.161.216.95.clients.your-server.de%3a6969%2fannounce&tr=udp%3a%2f%2fsu-data.com%3a6969%2fannounce&tr=udp%3a%2f%2ftamas3.ynh.fr%3a6969%2fannounce&tr=udp%3a%2f%2fthinking.duckdns.org%3a6969%2fannounce&tr=udp%3a%2f%2fthouvenin.cloud%3a6969%2fannounce&tr=udp%3a%2f%2ftk1.trackerservers.com%3a8080%2fannounce&tr=udp%3a%2f%2ftk1v6.trackerservers.com%3a8080%2fannounce&tr=udp%3a%2f%2ftorrents.artixlinux.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker-udp.gbitt.info%3a80%2fannounce&tr=udp%3a%2f%2ftracker.0x7c0.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.4.babico.name.tr%3a3131%2fannounce&tr=udp%3a%2f%2ftracker.artixlinux.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.auctor.tv%3a6969%2fannounce&tr=http%3a%2f%2ftracker.birkenwald.de%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.bittor.pw%3a1337%2fannounce&tr=udp%3a%2f%2ftracker.ccp.ovh%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.cubonegro.lol%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.cyberia.is%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.ddunlimited.net%3a6969%2fannounce&tr=http%3a%2f%2ftracker.dler.com%3a6969%2fannounce&tr=http%3a%2f%2ftracker.dler.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.dump.cl%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.filemail.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.fnix.net%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.moeking.me%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a6969%2fannounce&tr=http%3a%2f%2ftracker.opentrackr.org%3a1337%2fannounce&tr=udp%3a%2f%2ftracker.qu.ax%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.skyts.net%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.srv00.com%3a6969%2fannounce&tr=http%3a%2f%2ftracker.swateam.org.uk%3a2710%2fannounce&tr=udp%3a%2f%2ftracker.t-rb.org%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.theoks.net%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.therarbg.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.tiny-vps.com%3a6969%2fannounce&tr=udp%3a%2f%2ftracker.torrent.eu.org%3a451%2fannounce&tr=udp%3a%2f%2ftracker.tryhackx.org%3a6969%2fannounce&tr=http%3a%2f%2ftracker1.bt.moack.co.kr%3a80%2fannounce&tr=udp%3a%2f%2ftracker1.myporn.club%3a9337%2fannounce&tr=udp%3a%2f%2ftracker2.dler.com%3a80%2fannounce&tr=http%3a%2f%2ftracker2.dler.org%3a80%2fannounce&tr=udp%3a%2f%2ftracker2.itzmx.com%3a6961%2fannounce&tr=udp%3a%2f%2ftracker3.itzmx.com%3a6961%2fannounce&tr=http%3a%2f%2ftracker4.itzmx.com%3a2710%2fannounce&tr=udp%3a%2f%2fttk2.nbaonlineservice.com%3a6969%2fannounce&tr=udp%3a%2f%2fu4.trakx.crim.ist%3a1337%2fannounce&tr=udp%3a%2f%2fu6.trakx.crim.ist%3a1337%2fannounce&tr=udp%3a%2f%2fuploads.gamecoast.net%3a6969%2fannounce&tr=udp%3a%2f%2fv1046920.hosted-by-vdsina.ru%3a6969%2fannounce&tr=udp%3a%2f%2fv2.iperson.xyz%3a6969%2fannounce&tr=udp%3a%2f%2fvm3268418.24ssd.had.wf%3a6969%2fannounce&tr=http%3a%2f%2fwepzone.net%3a6969%2fannounce&tr=udp%3a%2f%2fyahor.of.by%3a6969%2fannounce&tr=ws%3a%2f%2fhub.bugout.link%3a80%2fannounce&tr=wss%3a%2f%2ftracker.openwebtorrent.com%3a443%2fannounce&tr=http%3a%2f%2f59.72.109.34%3a9000%2fannounce)


2. 将该镜像文件拷贝至目标虚拟机。

注：课程所提供之虚拟机已安装`open-vm-tools-desktop`，可以直接**拖动**文件至虚拟机内。你也可以使用其它方式获得镜像，在此不再赘述。

### **Step 2**: 创建 SSH 密钥

```shell
# 按照提示创建密钥
ssh-keygen

# 打开 ~/.ssh/id id_rsa.pub 并将其中内容提供给负责2号任务的同学
# 也可使用如下指令，把<ip02>替换为2号任务机ip
# ssh-copy-id root@<ip02>
```

### **Barrier 2**

### **Step 3**: 创建虚拟机以及安装操作系统

#### 挂载NFS服务器

```shell
# 挂载1号任务共享的目录，将<ip01>替换为1号任务所在机器的ip
mount -t nfs -o nolock <ip01>:/var/lib/libvirt-img/images /var/lib/libvirt/images
```

#### 创建虚拟机

以GNOME GUI为例

```shell
# 打开虚拟机管理软件VMM
virt-manager
```

连接到虚拟机服务，并创建VM（请注意课上演示）

![连接服务](assets/3_1.png)

![创建虚拟机](assets/3_2.png)

在这一步选择镜像安装方式。
![选择安装方式](assets/3_3.png)

请在这一步选择上一步所获取的OS安装镜像。
![选择镜像](assets/3_4.png)

![选择镜像](assets/3_5.png)

![选择镜像](assets/3_6.png)

![内存和CPU](assets/3_7.png)

硬盘大小约为8~15GB就足够安装OS了。
![硬盘](assets/3_8.png)

![命名](assets/3_9.png)

#### 在虚拟机中安装操作系统

**确保上一步中你已经挂载安装镜像为虚拟机的光盘。**

直接启动虚拟机，即可进入安装界面。此时根据页面提示**安装**操作系统即可。注意不要进入LiveCD临时预览。注意所有的不必要的软件（如第三方、可选更新等）都可以不安装以节省时间。

#### 修改虚拟机配置文件

**请确认已经关闭VM**，执行

```shell
# 列出虚拟机列表
virsh list --all

# 修改虚拟机配置，将<vm-name>替换为虚拟机名称
virsh edit <vm-name>
```

打开虚拟机`.xml`配置文件后，找到如下内容，添加 ```cache='none'``` 字段

```xml
<disk type='file' device='disk'>
      <!-- add: change cache mode to [none] --> 
      <driver name='qemu' type='qcow2' cache='none'/>
```

重新开启VM，并在VM中执行ping指令

```shell
ping 127.0.0.1 -i 10
```

### **Barrier 3**

### **Step 4**: 进行迁移

在宿主机执行以下指令

```shell
# 将<vm-name>替换为vm名称，将<ip02>替换为2号任务机器IP
virsh migrate --tunnelled --p2p --live <vm-name> qemu+ssh://root@<ip02>/system
```

### **Barrier 4**

### **Step 5**: 查看迁移结果

在2号任务机上查看迁移结果
