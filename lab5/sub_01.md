# lab5 - VM 迁移 - 1号任务

[上一级任务：lab5](task.md)

## 实验说明

3人一组，本文档为1号子任务，需要1名同学：建立 NFS 服务器

## 实验步骤

### **Step 1**: 创建共享目录并赋予权限

```shell
mkdir -p /var/lib/libvirt-img/images
chmod -R 777 /var/lib/libvirt-img/images
```

### **Step 2**: 使用 NFS 共享该目录

```shell
# 打开配置文件
vi /etc/exports

# 按i进入编辑模式，在文件末尾添加共享目录条目：
/var/lib/libvirt-img/images     <你的ip地址段>/<CIDR>(rw,no_root_squash,sync)

# 按esc退出编辑模式，输入:wq退出编辑器

# 关闭lock服务
systemctl stop nfs-lock

# 启用共享文件夹
exportfs -arv

# 检查目录共享是否正确，将<ip01>替换为本机ip
showmount -e <ip01>
```

### **Barrier 1**

### **Barrier 4**

### **Step 3**: 查看迁移结果

在2号任务机上查看迁移结果
