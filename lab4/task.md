# lab4 - MapReduce 编程

[实验课总目录](../README_zh.md)

[TOC]

## 实验说明

本次实验主要内容为在分布式环境下使用 MapReduce 编程。

本实验分为多人协作任务部分和单人任务部分。多人协作部分需要**3人一组**，共同完成多节点 Hadoop 运行环境配置任务。单人部分为实验主要内容，每人均需要独立进行 MapReduce 编程。单人部分代码编写完成后，使用多人协作部分的环境运行自己编写的代码。

## 评分标准

本次实验满分100分，将根据实验的完成度与实验步骤中各项任务的分值进行打分。

### 采分点

| 任务 | 采分点 | 分数 |
|-|-|-|
|准备工作| 部署 Hadoop 环境 | 50 |
|编写执行 Hadoop 程序| 数据求和 | 30 |
|编写执行 Hadoop 程序| WordCount | 20 |
|合计| | 100 |

## 虚拟机环境

虚拟机内环境已配置好，以下不需要执行，仅供参考。

```shell
yum install java-1.8.0-openjdk
yum install java-1.8.0-openjdk-devel

echo "export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.312.b07-1.el7_9.x86_64" >> /etc/profile
source /etc/profile

wget --no-check-certificate https://dlcdn.apache.org/hadoop/common/stable/hadoop-3.3.1.tar.gz
tar zxvf hadoop-3.3.1.tar.gz
mv hadoop-3.3.1 /usr/local/
chmod -R +x /usr/local/hadoop-3.3.1/

echo "export HADOOP_HOME=/usr/local/hadoop-3.3.1" >> /etc/profile
source /etc/profile
echo "export PATH=/usr/local/hadoop-3.3.1/bin:$PATH" >> /etc/profile
source /etc/profile
echo "export HADOOP_CLASSPATH=${JAVA_HOME}/lib/tools.jar" >> /etc/profile
source /etc/profile
```

## 实验内容

### 准备工作（多人协作任务，50分）

三人一组，开始前请先确定每位同学的序号（1、2、3）。

在开始各自的单人任务之前，**请先在每台虚拟机上各自进行以下准备工作**：

1. 更改基准虚拟机的MAC地址，将后两位替换为机器号的16进制编号

2. 在虚拟机内，修改hostname和ip等信息

    ```shell
    # 将<node0x>替换为node+两位组内序号（不足两位前边补0），如，组内1号同学为node01
    hostnamectl set-hostname <node0x>

    # 修改/etc/sysconfig/network-scripts/ifcfg-enp0s3中的IPADDR项，改为192.168.1.x，其中x是机器号

    # 重启
    reboot
    ```

#### **Barrier 1**

3. 修改 hosts 文件

    ```shell
    # 编辑/etc/hosts
    # 添加以下内容，分别将<ip01>、<ip02>、<ip03>替换为组内1、2、3号同学机器的ip
    <ip01> node01
    <ip02> node02
    <ip03> node03
    ```

4. 创建 SSH 密钥并拷贝到同组其他机器中

    ```shell
    # 按照提示创建密钥
    ssh-keygen

    # 将自己的密钥分别拷贝到各台机器上
    ssh-copy-id pd@node01
    ssh-copy-id pd@node02
    ssh-copy-id pd@node03

    # ssh到各台机器上检测是否能连接（连上以后exit退回）
    ssh pd@node01
    ssh pd@node02
    ssh pd@node03
    ```

5. 修改配置文件

    ```shell
    # 进入Hadoop配置文件目录
    cd $HADOOP_HOME/etc/hadoop/
    ```

    编辑 ```core-site.xml```，在```<configuration>```节点中加入配置

    ```xml
    <property>
        <name>fs.defaultFS</name>
        <value>hdfs://node01:9000</value>
    </property>
    ```

    编辑 ```yarn-site.xml```，在```<configuration>```节点中加入配置

    ```xml
    <property>
        <name>yarn.resourcemanager.hostname</name>
        <value>node01</value>
    </property>
    <property>
        <name>yarn.nodemanager.vmem-check-enable</name>
        <value>false</value>
    </property>
    <property>
        <name>yarn.log-aggregation-enable</name>
        <value>true</value>
    </property>
    <property>
        <name>yarn.log-aggregation.retain-seconds</name>
        <value>86400</value>
    </property>
    <property>
        <name>yarn.nodemanager.aux-services</name>
        <value>mapreduce_shuffle</value>
    </property>
    ```

    编辑 ```mapred-site.xml```，在```<configuration>```节点中加入配置

    ```xml
    <property>
        <name>mapreduce.jobhistory.address</name>
        <value>node01:10020</value>
    </property>
    <property>
        <name>mapreduce.jobhistory.webapp.address</name>
        <value>node01:19888</value>
    </property>
    <property>
        <name>mapreduce.framework.name</name>  
        <value>yarn</value>  
    </property>
    <property>
        <name>yarn.app.mapreduce.am.env</name>
        <value>HADOOP_MAPRED_HOME=/usr/local/hadoop-3.3.1</value>
    </property>
    <property>
        <name>mapreduce.map.env</name>
        <value>HADOOP_MAPRED_HOME=/usr/local/hadoop-3.3.1</value>
    </property>
    <property>
        <name>mapreduce.reduce.env</name>
        <value>HADOOP_MAPRED_HOME=/usr/local/hadoop-3.3.1</value>
    </property>
    ```

    编辑 ```workers```，内容为

    ```shell
    node01
    node02
    node03
    ```

    编辑 ```hadoop-env.sh```，添加一行

    ```shell
    export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.312.b07-1.el7_9.x86_64
    ```

#### **Barrier 2**

6. **在 node01 上**初始化 Hadoop HDFS

    ```shell
    cd $HADOOP_HOME/bin

    # 初始化namenode
    ./hadoop namenode -format

    # 此时在/tmp/hadoop-pd/dfs/name/current下可以看到内容
    ```

7. **在 node01 上**开启/关闭 Hadoop 服务

    **开启**所有服务

    ```shell
    # 开启全部hadoop服务
    cd $HADOOP_HOME/sbin

    ./start-all.sh
    ./mr-jobhistory-daemon.sh start historyserver
    ```

    此时浏览器访问```http://node01:9870```和```http://node01:8088```应当能看到hdfs和yarn的信息页面

    要**结束**所有服务，运行

    ```shell
    $HADOOP_HOME/sbin/stop-all.sh
    $HADOOP_HOME/sbin/mr-jobhistory-daemon.sh stop historyserver
    ```

### 实验步骤（单人任务，50分）

#### **Step 1**: 编写 MapReduce 程序

请分别按照以下要求编写 MapReduce 程序：

1. **(30分)** 使用 MapReduce 编写 数据求和 例程
2. **(20分)** 使用 MapReduce 编写 WordCount 例程

注意，由于以上任务分值递减，请按照**从前到后**的顺序完成以上任务

每一项编程任务均需要能够运行得到结果（Step 2、3）才能得到分数

#### **Step 2**: 打包程序

得分必要项

例如，程序文件名为 ```WordCount.java```

```shell
hadoop com.sun.tools.javac.Main WordCount.java
jar cf wc.jar WordCount*.class
```

#### **Step 3**: 执行程序并查看结果

得分必要项

在 HDFS 中创建输入目录并将输入文件 ```./wc.txt``` 放入该目录

```shell
hadoop fs -mkdir /wc
hadoop fs -mkdir /wc/input
hadoop fs -put ./wc.txt /wc/input
```

在 Hadoop 集群上运行程序

```shell
hadoop jar wc.jar WordCount /wc/input /wc/output
```

查看执行结果

```shell
# 查看输出文件列表
hadoop fs -ls /wc/output

# 查看输出文件内容，将<output_file>替换为上一条指令查询到的文件名
hadoop fs -cat /wc/output/<output_file>
```
