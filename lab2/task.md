# lab2 - OpenMP 编程

[实验课总目录](../README_zh.md)

[TOC]

## 实验说明

本次实验主要内容为在多核环境下编写、编译并执行 OpenMP 程序。

本实验为单人任务。

## 评分标准

本次实验满分100分，将根据实验的完成度与实验步骤中各项任务的分值进行打分。

### 采分点

| 任务 | 采分点 | 分数 |
|-|-|-|
|编写执行 OpenMP 程序| parallel 制导 | 30 |
|编写执行 OpenMP 程序| for 制导或 section 制导 | 30 |
|编写执行 OpenMP 程序| 矩阵加法 | 15 |
|编写执行 OpenMP 程序| 矩阵乘法 | 15 |
|编写执行 OpenMP 程序| 矩阵乘法优化 | 10 |
|合计| | 100 |

## 虚拟机环境

虚拟机内环境已配置好，以下不需要执行，仅供参考。

```shell
yum -y install gcc gcc-c++
```


## 实验内容

### 准备工作

1. 下载首页提供的已打包的虚拟机文件`pdlab_baseline.zip`，其所有实验运行环境已经预先准备完毕；
2. 使用VirtualBox导入该虚拟机；
3. 开机后进入虚拟机系统开始实验。

### 实验步骤

#### **Step 1**: 编写 OpenMP 程序

请分别按照以下要求编写 OpenMP 程序（注意各任务分值，满分100分）：

1. **(30分)** 编写代码测试 parallel 制导
2. **(30分)** 编写代码测试 for 制导或 section 制导（二选一）
3. **(15分)** 设计并编写用于计算矩阵*加法*的 OpenMP 并行代码
4. **(15分)** 设计并编写用于计算矩阵*乘法*的 OpenMP 并行代码
5. **(10分)** 尝试对矩阵乘法进行优化并说明

注意，由于以上任务分值递减，请按照**从前到后**的顺序完成以上任务

4、5号任务可以只提供一份代码，只要能够说明该代码中优化思路即可获得任务5的分数

每一项编程任务均需要能够通过编译并运行得到结果（Step 2、3）才能得到分数

#### **Step 2**: 编译程序

得分必要项

```shell
# 将test.c替换为你的代码文件名，test.o是输出的可执行文件名
gcc -fopenmp -o test.o test.c
```

#### **Step 3**: 执行程序并查看结果

得分必要项

```shell
# 运行上一步中生成的可执行文件
./test.o
```
