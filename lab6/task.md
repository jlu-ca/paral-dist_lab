# lab6 - 云计算实践

[实验课总目录](../README_zh.md)

[TOC]

## 实验说明

本次实验主要任务为设计、实现并部署一套云上的服务程序（例如一个动态网站）。任务可以划分为：编写云服务程序（微服务形式）、搭建云服务的基础设施并将微服务部署在搭建的云服务基础设施上。

云服务的内容可以自己决定，例如社交或购物网站等。该云服务需要由若干个（不少于三个）微服务构成。请先对云服务的服务内容、微服务的划分、微服务之间交互的接口等架构性内容进行设计，然后再开始实现微服务。

本次实验为多人协作任务，且分为两个实验部分（lab6A, lab6B）。全部实验需要**6人一大组，其中每2人一小组**。

各实验部分详细内容请参考对应页面：

- [lab6A: 以微服务方式实现 web service](lab6A/task.md)
- [lab6B: 使用 Docker 和 Kubernetes 进行微服务部署](lab6B/task.md)

## 评分标准

本次实验满分100分，其中 lab6A 占50分，lab6B 占50分。

### 采分点

| 任务 | 采分点 | 分数 |
|-|-|-|
|lab6A| 完成度 | 50 |
|lab6B| 演示 | 50 |
|合计| | 100 |

## 环境

虚拟机内环境已配置好，以下不需要执行，仅供参考

  ```shell
  swapoff -a
  # comment out the swap line in /etc/fstab

  cat <<EOF | sudo tee /etc/modules-load.d/k8s.conf
  br_netfilter
  EOF

  cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
  net.bridge.bridge-nf-call-ip6tables = 1
  net.bridge.bridge-nf-call-iptables = 1
  EOF
  sysctl --system

  curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun

  mkdir /etc/docker
  cat <<EOF | sudo tee /etc/docker/daemon.json
  {
    "exec-opts": ["native.cgroupdriver=systemd"],
    "log-driver": "json-file",
    "log-opts": {
      "max-size": "100m"
    },
    "storage-driver": "overlay2"
  }
  EOF

  systemctl enable docker
  systemctl daemon-reload
  systemctl restart docker

  cat <<EOF > /etc/yum.repos.d/kubernetes.repo
  [kubernetes]
  name=Kubernetes
  baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
  enabled=1
  gpgcheck=1
  repo_gpgcheck=1
  gpgkey=https://mirrors.aliyun.com/kubernetes/yum/doc/yum-key.gpg https://mirrors.aliyun.com/kubernetes/yum/doc/rpm-package-key.gpg
  EOF

  yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

  systemctl daemon-reload

  # kubeadm config images pull

  ./pull-kube-images.sh

  wget https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

  wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.0/deploy/static/provider/cloud/deploy.yaml -O ingress-nginx.yaml

  # 编辑文件，加入内容
  # 在ingress-nginx-controller的spec/template/spec/下加入hostNetwork: true
  # 在ingress-nginx-controller的spec/template/spec/containers/[0]/ports/[containerPort==80]处加入hostPort: 80
  # 将ingress-nginx-controller的kind改为DaemonSet

  docker pull rancher/mirrored-flannelcni-flannel-cni-plugin:v1.0.0

  docker pull mysql
  docker pull alpine
  docker pull ubuntu

  # 可能需要先配置代理
  docker pull k8s.gcr.io/ingress-nginx/controller:v1.1.0@sha256:f766669fdcf3dc26347ed273a55e754b427eb4411ee075a53f30718b4499076a
  docker pull k8s.gcr.io/ingress-nginx/kube-webhook-certgen:v1.1.1@sha256:64d8c73dca984af206adf9d6d7e46aa550362b1d7a01f3a0a91b20cc67868660
  ```

docker proxy 代理配置
```/etc/systemd/system/docker.service.d/http_proxy.conf```

  ```conf
  [Service]
  Environment="HTTP_PROXY=192.168.1.9:8001"
  Environment="HTTPS_PROXY=192.168.1.9:8001"
  ```

  ```shell
  systemctl daemon-reload && systemctl restart docker
  ```

为避免无法拉取谷歌官方镜像，可以使用的脚本 ```pull-kube-images.sh```

  ```shell
  #!/bin/bash
  K8S_VERSION=v1.23.1
  ETCD_VERSION=3.5.1-0
  DASHBOARD_VERSION=v2.2.0
  FLANNEL_VERSION=v0.15.1
  DNS_VERSION=v1.8.6
  PAUSE_VERSION=3.6

  # 基本组件
  docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-apiserver-amd64:$K8S_VERSION
  docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-controller-manager-amd64:$K8S_VERSION
  docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-scheduler-amd64:$K8S_VERSION
  docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/kube-proxy-amd64:$K8S_VERSION
  docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/etcd:$ETCD_VERSION
  docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/pause:$PAUSE_VERSION
  docker pull registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:$DNS_VERSION

  # 网络组件
  docker pull quay.io/coreos/flannel:$FLANNEL_VERSION

  # 修改tag
  docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/kube-apiserver-amd64:$K8S_VERSION k8s.gcr.io/kube-apiserver:$K8S_VERSION
  docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/kube-controller-manager-amd64:$K8S_VERSION k8s.gcr.io/kube-controller-manager:$K8S_VERSION
  docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/kube-scheduler-amd64:$K8S_VERSION k8s.gcr.io/kube-scheduler:$K8S_VERSION
  docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/kube-proxy-amd64:$K8S_VERSION k8s.gcr.io/kube-proxy:$K8S_VERSION
  docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/etcd:$ETCD_VERSION k8s.gcr.io/etcd:$ETCD_VERSION
  docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/pause:$PAUSE_VERSION k8s.gcr.io/pause:$PAUSE_VERSION
  docker tag registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:$DNS_VERSION k8s.gcr.io/coredns/coredns:$DNS_VERSION

  # 删除冗余的images
  docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/kube-apiserver-amd64:$K8S_VERSION
  docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/kube-controller-manager-amd64:$K8S_VERSION
  docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/kube-scheduler-amd64:$K8S_VERSION
  docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/kube-proxy-amd64:$K8S_VERSION
  docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/etcd:$ETCD_VERSION
  docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/pause:$PAUSE_VERSION
  docker rmi registry.cn-hangzhou.aliyuncs.com/google_containers/coredns:$DNS_VERSION
  ```

## 准备工作

在开始各自的实验之前，**请先在每台虚拟机上进行以下准备工作**

建议实验全部操作在root账户下执行

  ```shell
  su root
  ```

1. 更改基准虚拟机的MAC地址，将后两位替换为机器号的16进制编号

2. 在虚拟机内，修改hostname和ip等信息

   ```shell
   # 将<hostname>替换为pd+三位十进制机器号（不足三位前边补0），如pd001
   hostnamectl set-hostname <hostname>

   # 修改/etc/sysconfig/network-scripts/ifcfg-enp0s3中的IPADDR项，改为192.168.1.x，其中x是机器号

   # 重启
   reboot
   ```

3. 修改 ```/etc/hosts``` 文件，为大组内每位同学的虚拟机均添加一行host解析条目

   ```shell
   # 将<ip>替换为对应机器的ip，将<hostname>替换为对应机器的hostname，如
   # 192.168.1.1 pd001
   <ip> <hostname>
   ```
