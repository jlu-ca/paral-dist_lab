# lab6B - 使用 Docker 和 Kubernetes 进行微服务部署

[上一级任务：lab6](../task.md)

[上一项任务：lab6A](../lab6A/task.md)

[TOC]

## 实验说明

在进行本实验前，请先阅读 [lab6](../task.md) 的实验说明。请在完成小组任务微服务设计和实现（[lab6A](../lab6A/task.md)）后再开始进行本实验。

云服务基础设施的搭建需要在提供的基准虚拟机中配置 Kubernetes 环境，并将**6人大组**内全部机器组织为一个集群（lab6B）。此后，将写好的微服务部署到该集群中，并对其进行扩容实验。服务发现和负载均衡可以使用 Kubernetes 提供的 Service 功能。

可以使用 Ingress 为集群设置流量出入口，但这一项不是必要的，也不作为采分点。

## 评分标准

本次实验满分50分。能够成功将微服务部署到集群上，可以获得30分。能够对微服务进行扩容、缩容，并能展示出对微服务的访问来自不同的pod，视情况给予更高分数。

## 实验步骤

### **Step 1**: 部署启动 Kubernetes 集群

组内全部虚拟机上运行：

```shell
systemctl enable kubelet
```

### **Barrier 1**

在大组内**1号同学**的虚拟机上执行以下指令，这将初始化一个 Kubernetes 集群，并使得1号同学的虚拟机成为控制节点。

```shell
kubeadm init --pod-network-cidr=10.244.0.0/16
```

将屏幕上输出的包含 ```kubeadm join``` 的指令记录下来供下一步使用

然后执行

```shell
echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> /etc/profile
source /etc/profile
# 配置 flannel 网络插件
kubectl apply -f /home/pd/kube-flannel.yml
```

### **Barrier 2**

在大组内除1号外全部同学的虚拟机上执行上一步在1号机记录的指令，加入集群，形如：

```shell
# 以下仅为示例，请将具体指令替换为上一步中记录的指令
kubeadm join 192.168.1.111:6443 --token p3mwz5.517lce0iy45bphf7 \
 --discovery-token-ca-cert-hash sha256:6c00efd9676476800f3619399163a290bcbbd2846509d53cf465db3eb49ff761
```

### **Barrier 3**

### **Step 2**: 在集群上部署微服务

为每一个微服务编写 yml 文件，配置微服务的 Deployment 和 Service，尖括号部分的内容请自行替换

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: <deployment-name>
spec:
  selector:
    matchLabels:
      app: <app-label>
  replicas: 3
  template:
    metadata:
      labels:
        app: <app-label>
    spec:
      containers:
      - name: <container-name>
        image: <microservice-image-name>
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 8080
        env:
        - name: MY_NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: MY_POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: MY_POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: MY_POD_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        - name: MY_POD_SERVICE_ACCOUNT
          valueFrom:
            fieldRef:
              fieldPath: spec.serviceAccountName
              
---

apiVersion: v1
kind: Service
metadata:
  name: <service-name>
spec:
  selector:
    app: <app-label>
  ports: # 以基于HTTP协议的微服务接口为例
  - protocol: TCP
    port: 80
    targetPort: 8080
```

在大组1号同学虚拟机（控制节点）上执行以下语句，将微服务部署到集群上

```shell
kubectl apply -f <yml-file(s)>
```

### **Step 3**: 对微服务进行扩容

修改微服务 yml 文件，将 ```replicas``` 字段的值增大

在大组1号同学虚拟机（控制节点）上执行以下语句，更新微服务部署

```shell
kubectl apply -f <yml-file(s)>
```

## 举例

接续 lab6A 中举例部分

编写 ```ping-pong.yml```

```yml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ping-pong-deployment
spec:
  selector:
    matchLabels:
      app: ping-pong
  replicas: 3
  template:
    metadata:
      labels:
        app: ping-pong
    spec:
      containers:
      - name: ping-pong
        image: ping-pong
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 8080
        env:
        - name: MY_NODE_NAME
          valueFrom:
            fieldRef:
              fieldPath: spec.nodeName
        - name: MY_POD_NAME
          valueFrom:
            fieldRef:
              fieldPath: metadata.name
        - name: MY_POD_NAMESPACE
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
        - name: MY_POD_IP
          valueFrom:
            fieldRef:
              fieldPath: status.podIP
        - name: MY_POD_SERVICE_ACCOUNT
          valueFrom:
            fieldRef:
              fieldPath: spec.serviceAccountName
              
---

apiVersion: v1
kind: Service
metadata:
  name: ping-pong-service
spec:
  selector:
    app: ping-pong
  ports:
  - protocol: TCP
    port: 80
    targetPort: 8080

```

应用上述配置

```shell
kubectl apply -f ping-pong.yml
```

等待全部 pod 开启后，连接到任意一个 pod

```shell
kubectl get pods
kubectl exec -it ping-pong-deployment-98895dcb4-n5pkb -- bash
```

在 pod 内执行

```shell
apt-get update
apt-get install curl

# 访问service
curl ping-pong-service/ping

# output: {"from":"10.244.2.14","message":"pong"}
```

多次执行可以发现 from 字段不同，也就是响应来自于不同的 pod

编辑 ```ping-pong.yml```，将 replicas 的值改为 5

然后再次执行

```shell
kubectl apply -f ping-pong.yml
```

此时使用 ```kubectl get pods``` 可以发现运行的 pod 数量增加到 5 了，且在 pod 内执行 ```curl ping-pong-service/ping``` 也能看到新的地址，说明该服务已经扩容成功
