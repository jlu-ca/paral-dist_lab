# lab6A - 以微服务方式实现 web service

[上一级任务：lab6](../task.md)

[TOC]

## 实验说明

在进行本实验前，请先阅读 [lab6](../task.md) 的实验说明。

本实验主要为设计并实现微服务以供下一步云服务部署使用。服务内容和系统架构的设计部分由6人大组共同完成，此后每个2人小组负责一个微服务的实现。

每个大组需要实现至少三个不同的微服务。微服务有实际的应用意义，并能够根据其负载、耦合关系进行合理划分，将能够获得更高的分数。最低限度的要求（30分）是设计并实现三个能够互相通过网络调用彼此接口的微服务原型程序。

实际应用意义和合理的划分的例子：以购物网站为例，可以将其划分为以下3个微服务：账户服务、商品信息服务、交易服务。

可以自由选择编程语言和框架。例如，使用 Java & Spring Cloud，或是使用 Go & gRPC 等。但前提是，大组内各微服务之间应当能够互相正确通信（建议同一大组内使用一套语言和框架），应当能够配合 Docker & Kubernetes 进行容器化部署，同时能够保证可多节点部署、可动态伸缩。这就需要在设计架构时预先设计好全部接口，且应考虑微服务是否具有“状态”。

可以自由选择要使用的系统软件和中间件，基准虚拟机中只提供 MySQL 的 Docker 镜像，其他软件需要自行安装。也可以不使用数据库，只使用虚构的静态数据做演示。

## 评分标准

本次实验满分50分。能够完成基本要求，设计并实现三个的微服务，且微服务之间至少有两次相互调用，可以获得30分。能够为服务赋予应用意义，并根据实际情况对微服务进行设计、划分的，视情况给予更高分数。

## 实验步骤

### **Step 1**: 架构设计任务

首先对云服务的架构进行设计，要求**大组内6人共同完成**以下分析和设计并提供文字内容：

- **需求分析**：研究确定要提供的服务，清晰描述服务能够为用户所提供的的功能。

    参考工具：用例图等

- **概要设计**：确定系统及各个模块（微服务）所使用的技术，包括编程语言、框架、软件、中间件等；设计整个系统的结构，包括模块（微服务）划分、模块（微服务）之间的调用关系、系统以及各模块（微服务）的输入输出、接口等；设计系统中的数据结构设计，包括数据库设计、服务中的数据对象设计、缓存（如果有）数据设计等。

    参考工具：系统架构图、组件图、时序图、ER图、接口代码等

- **详细设计**：细化设计每个模块（微服务）内部的流程、算法等。

    参考工具：流程图等

在撰写文档过程中不拘泥于具体工具，但需要能描述清晰，并指导后续工作中微服务的实现。以上三部分内容可以合并为一份文档。

完成架构设计后，请确定每个2人小组负责哪个（些）微服务的实现，并体现在文档中。

### **Step 2**: 微服务实现

以2人小组为单位，实现上述微服务并进行测试。这一步可以在自己电脑上进行，待编写完成后打包成 docker 镜像拷贝到虚拟机即可。若在虚拟机内编写，需要自行配置环境。基准虚拟机内只提供了 openjdk 1.8.0_312-b07。

### **Step 3**: 将微服务打包为 Docker 镜像

启动docker

```shell
systemctl start docker
```

使用 Dockerfile 打包镜像，基准虚拟机内提供 alpine 和 ubuntu 的 Docker 镜像，可以用来作为打包的基础镜像

```Dockerfile
# 按需编写
FROM ubuntu
COPY ...
CMD ...
...
```

导出镜像并将镜像在虚拟机上导入（由于我们没有搭建私有 docker 镜像仓库，因此请将镜像导入到大组内所有的虚拟机上以供下一次实验使用）

```shell
# 导出
docker save foo > foo.tar

# 导入
docker load < foo.tar
```

## 举例

注意：虚拟机内未安装 Golang 环境和相关 module，此处仅为演示，如需使用请先按以下内容自行配置环境

```shell
# 环境安装
wget https://golang.google.cn/dl/go1.17.5.linux-amd64.tar.gz
tar -zxvf go1.17.5.linux-amd64.tar.gz 
mv go /usr/local/
export PATH=/usr/local/go/bin:$PATH

yum install git

go env -w GO111MODULE=auto
go get -u github.com/gin-gonic/gin
```

使用 go 编写服务 ```ping-pong.go```

```go
package main

import "github.com/gin-gonic/gin"
import "os"

func main() {
 myname := os.Getenv("MY_POD_IP");
 r := gin.Default()
 r.GET("/ping", func(c *gin.Context) {
  c.JSON(200, gin.H{
   "message": "pong",
   "from": myname,
  })
 })
 r.Run() // listen and serve on 0.0.0.0:8080
}
```

编译

```shell
go build ping-pong.go
```

本地运行和测试

```shell
# 运行
./ping-pong

# 测试
curl localhost:8080/ping
# output: {"from":"","message":"pong"}
# from字段为空是正常情况，其读取的MY_POD_IP环境变量将在下节实验中可用
```

编写 Dockerfile

```Dockerfile
FROM ubuntu
COPY ping-pong /bin/
RUN chmod 777 /bin/ping-pong
CMD /bin/ping-pong
```

打包

```shell
docker build -t ping-pong .
```

一般来讲，使用 k8s 需要使用公共或自建的 docker 仓库来存放镜像，在此为简化操作我们不搭建仓库，而是手动将镜像同步到其他节点

将镜像导出

```shell
docker save ping-pong > ping-pong-img.tar
```

将导出的镜像在组内全部虚拟机上导入

```shell
docker load < ping-pong-img.tar
```
